<?php


namespace Shavy\QuickCache;



@include __DIR__."/../../../vendor/autoload.php";

class QCache {

    protected static $global_id=array();
    private $save_dir=".qcache";
    protected $_id;
    protected $keys;

    public function __construct(){
        if(count(self::$global_id)==0){
            if(is_dir($this->save_dir())){
                $this->rm_folder_recursively($this->save_dir());
            }
        }
    }

    private function rm_folder_recursively($dir) {
        foreach(scandir($dir) as $file) {
            if ('.' === $file || '..' === $file) continue;
            if (is_dir("$dir/$file")) $this->rm_folder_recursively("$dir/$file");
            else unlink("$dir/$file");
        }
        rmdir($dir);
        return true;
    }

    public function all(){
        $this->import();
        $answer=$this->keys;
        $this->export();
        return $answer;
    }


    private function generate_id($key){
        $_id=uniqid($key). (string) rand(0,1000);
        if($this->array_contains($_id)){
            $_id=$this->generate_id($key);
        }
        $this->_id=$_id;
        return $_id;
    }

    public function set_text($key,$value){
        if($this->_id==null){
            $this->generate_id($key);
        }

        $this->import();
        $this->keys[$key]=$value;
        $this->export();
    }


    private function export(){
        $content=json_encode($this->keys);
        file_put_contents($this->filename(),$content);
        $this->keys=array();
    }

    private function import(){
        if(isset($this->keys) && file_exists($this->filename())){
            $content=file_get_contents($this->filename());
            $this->keys=json_decode($content,true);
        }else{
            $this->keys=array();
        }
    }

    public function get_text($key){
        $value=null;
        $this->import();
        if(isset($this->keys[$key])){
            $value= $this->keys[$key];
        }
        $this->export();
        return $value;
    }

    public function array_contains($key){
        foreach(self::$global_id as $id){
            if($id===$key)
                return true;
        }
        return false;
    }

    public function save_dir(){
        return sys_get_temp_dir();
    }

    public function filename(){
        if(!is_dir($this->save_dir())){
            mkdir($this->save_dir());
        }
        return $this->save_dir()."/".$this->_id."json";
    }

    public function remove_key(){
        for($i=0,$count=count(self::$global_id);$i<$count;$i++){
            if($this->_id===self::$global_id[$i]){
                unset(self::$global_id[$i]);
                self::$global_id[$i]=array_values(self::$global_id[$i]);
                break;
            }
        }
        if(count(self::$global_id)==0){
            rmdir($this->save_dir());
        }
    }

    public function __destruct(){
        unlink($this->filename());
        $this->remove_key();
    }
}