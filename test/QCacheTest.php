<?php
/**
 * Created by JetBrains PhpStorm.
 * User: shavyg2
 * Date: 1/30/14
 * Time: 2:11 AM
 * To change this template use File | Settings | File Templates.
 */


use Shavy\QuickCache\QCache;
class QCacheTest extends PHPUnit_Framework_TestCase{

    public function test_id_index_filesave(){
        $quick1= new QCache;
        $quick1->set_text("hello_world","come back to me");

        $filename=$quick1->filename();

        $this->assertTrue(file_exists($quick1->filename()));
        $this->assertEquals("come back to me",$quick1->get_text("hello_world"));

        unset($quick1);
        $this->assertTrue(!file_exists($filename));
    }

}